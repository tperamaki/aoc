(ns aoc.core
  (:require [clj-http.client :as client]
            [clojure.data.json :as json]))

(defn extractJson
  [responseBody]
  (get (json/read-str responseBody) "value1"))

(defn makeCall
  [url input]
   (:body
    (client/get url
                {:query-params {"value1" input}})))

(defn getIt
  [url input]
   (println
    (extractJson
     (makeCall url input))))

(defn -main 
  "Command-line entry point."
  [& raw-args] (getIt "http://adventofcode.com/2016/day/1/input" "test1"))
