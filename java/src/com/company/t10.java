package com.company;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class t10 {
  private static final String input = "83,0,193,1,254,237,187,40,88,27,2,255,149,29,42,100";

  private static int position = 0;
  private static int skipSize = 0;

  public static void run() {
    ArrayList<Integer> list = new ArrayList<>();
    for (int i = 0; i < 256; i++) {
      list.add(i);
    }
    ArrayList<Integer> lengths = new ArrayList<>();
    String[] stringLengths = input.split(",");
    for (int i = 0; i < stringLengths.length; i++) {
      lengths.add(Integer.parseInt(stringLengths[i]));
    }
    List<Integer> listAnswerT1 = t1(list, lengths);
    System.out.println("t1: " + (listAnswerT1.get(0) * listAnswerT1.get(1)));

    list = new ArrayList<>();
    for (int i = 0; i < 256; i++) {
      list.add(i);
    }
    t2(list);
  }

  private static List<Integer> t1(List<Integer> list, List<Integer> lengths) {

    for (int lengthIndex = 0; lengthIndex < lengths.size(); lengthIndex++) {
      Integer length = lengths.get(lengthIndex);
      for (int i = 0; i < length / 2; i++) {
        Integer roundI = (i + position) % 256;
        Integer temp = list.get(roundI);
        list.set(roundI, list.get((position+length-i-1) % 256));
        list.set((position+length-i-1) % 256, temp);
      }
      position += (skipSize+length);
      position = position % 256;
      skipSize++;
    }
    return list;
  }

  public static void t2(List<Integer> list) {
    skipSize = 0;
    position = 0;
    Integer[] lengthSuffix = { 17, 31, 73, 47, 23 };
    char[] chars = input.toCharArray();
    List<Integer> asciiList = new ArrayList<>();
    for (int i = 0; i < chars.length; i++) {
      asciiList.add((int) chars[i]);
    }
    Collections.addAll(asciiList, lengthSuffix);

    for (int i = 0; i < 64; i++) {
      list = t1(list, asciiList);
    }

    List<Integer> xorList = new ArrayList<>();
    for (int i = 0; i < 16; i++) {
      Integer temp = list.get(i*16);
      for (int j = 1; j < 16; j++) {
        temp = temp ^ list.get(i*16+j);
      }
      xorList.add(temp);
    }

    String hash = xorList.stream().map(Integer::toHexString).collect(Collectors.joining(""));
    System.out.println(hash);
  }
}
