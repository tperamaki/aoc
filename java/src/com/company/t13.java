package com.company;

import java.util.Arrays;
import java.util.HashMap;

public class t13 {
  private static final String input = "0: 3\n" +
      "1: 2\n" +
      "2: 4\n" +
      "4: 6\n" +
      "6: 4\n" +
      "8: 6\n" +
      "10: 5\n" +
      "12: 6\n" +
      "14: 8\n" +
      "16: 8\n" +
      "18: 8\n" +
      "20: 6\n" +
      "22: 12\n" +
      "24: 8\n" +
      "26: 8\n" +
      "28: 10\n" +
      "30: 9\n" +
      "32: 12\n" +
      "34: 8\n" +
      "36: 12\n" +
      "38: 12\n" +
      "40: 12\n" +
      "42: 14\n" +
      "44: 14\n" +
      "46: 12\n" +
      "48: 12\n" +
      "50: 12\n" +
      "52: 12\n" +
      "54: 14\n" +
      "56: 12\n" +
      "58: 14\n" +
      "60: 14\n" +
      "62: 14\n" +
      "64: 14\n" +
      "70: 10\n" +
      "72: 14\n" +
      "74: 14\n" +
      "76: 14\n" +
      "78: 14\n" +
      "82: 14\n" +
      "86: 17\n" +
      "88: 18\n" +
      "96: 26";

  private static HashMap<Integer, Integer> inputData = new HashMap<>();

  public static void run() {
    Arrays.asList(input.split("\n")).forEach(row -> inputData.put(Integer.parseInt(row.split(": ")[0]), Integer.parseInt(row.split(": ")[1])));
    t1();
    t2();
  }


  private static void t1() {
    int severity = 0;
    for (int i = 0; i < inputData.size()+100; i++) {
      if (inputData.get(i) != null && i % ((inputData.get(i) - 1) * 2) == 0) {
        severity += i * inputData.get(i);
      }
    }
    System.out.println(severity);
  }

  private static void t2() {
    int severity = -1;
    for (int j = 0; severity != 0; j++) {
      severity = 0;
      for (int i = 0; i < inputData.size()+100; i++) {
        if (inputData.get(i) != null && (i+j) % ((inputData.get(i) - 1) * 2) == 0) {
          severity++;
        }
      }
      System.out.println(severity);
      System.out.println(j);
    }
  }
}
