package com.company;

import java.math.BigInteger;
import java.util.*;
import java.util.stream.Collectors;

public class t14 {
  private static final String input = "hfdlxzhv";
  //private static final String input = "flqrgnkx"; // 8108 // 1242

  private static HashMap<Integer, Integer> inputData = new HashMap<>();
  private static int position = 0;
  private static int skipSize = 0;
  private static List<List<String>> list = new ArrayList<>();

  public static void run() {
    t1();
    t2();
  }


  private static void t1() {
    int sum = 0;
    for (int i = 0; i < 128; i++) {
      String hexHash = knot(input + "-" + i);
      String binaryHash = "";
      for (int j = 0; j < hexHash.length(); j++) {
        String bin = new BigInteger(hexHash.substring(j, j+1), 16).toString(2);
        while (bin.length() < 4) {
          bin = "0" + bin;
        }
        binaryHash += bin;
      }
      sum += Arrays.stream(binaryHash.split("")).filter(b -> Integer.parseInt(b) == 1).count();
    }
    System.out.println(sum);
  }

  private static void t2() {
    int sum = 0;
    for (int i = 0; i < 128; i++) {
      String hexHash = knot(input + "-" + i);
      String binaryHash = "";
      for (int j = 0; j < hexHash.length(); j++) {
        String bin = new BigInteger(hexHash.substring(j, j+1), 16).toString(2);
        while (bin.length() < 4) {
          bin = "0" + bin;
        }
        binaryHash += bin;
      }
      list.add(i, new ArrayList<>());
      Collections.addAll(list.get(i), binaryHash.split(""));
      if (binaryHash.length() != 128)
        System.out.println("error1");
      if (list.get(i).size() != 128)
        System.out.println("error2");
    }
    if (list.size() != 128)
      System.out.println("error3");
    for (int i = 0; i < 128; i++) {
      for (int j = 0; j < 128; j++) {
        if (list.get(i).get(j).equals("1")) {
          sum++;
          removeGroup(i, j);
        }
      }
    }
    System.out.println(sum);
  }

  private static void removeGroup(int i, int j) {
    list.get(i).set(j, "0");
    if (i > 0 && list.get(i-1).get(j).equals("1"))
      removeGroup(i-1, j);
    if (j > 0 && list.get(i).get(j-1).equals("1"))
      removeGroup(i, j-1);
    if (i < list.size()-1 && list.get(i+1).get(j).equals("1"))
      removeGroup(i+1, j);
    if (j < list.size()-1 && list.get(i).get(j+1).equals("1"))
      removeGroup(i, j+1);
  }

  private static String knot(String input) {
    position = 0;
    skipSize = 0;

    List<Integer> list = new ArrayList<>();
    for (int i = 0; i < 256; i++) {
      list.add(i);
    }

    Integer[] lengthSuffix = { 17, 31, 73, 47, 23 };
    char[] chars = input.toCharArray();
    List<Integer> asciiList = new ArrayList<>();
    for (int i = 0; i < chars.length; i++) {
      asciiList.add((int) chars[i]);
    }
    Collections.addAll(asciiList, lengthSuffix);

    for (int i = 0; i < 64; i++) {
      list = knot_t1(list, asciiList);
    }

    List<Integer> xorList = new ArrayList<>();
    for (int i = 0; i < 16; i++) {
      Integer temp = list.get(i*16);
      for (int j = 1; j < 16; j++) {
        temp = temp ^ list.get(i*16+j);
      }
      xorList.add(temp);
    }

    String hash = xorList.stream().map(Integer::toHexString).map(x -> x.length() == 1 ? "0" + x : x).collect(Collectors.joining(""));
    if (hash.length() != 32) {
      System.out.println("HASHERROR");
    }
    return hash;
  }

  private static List<Integer> knot_t1(List<Integer> list, List<Integer> lengths) {

    for (int lengthIndex = 0; lengthIndex < lengths.size(); lengthIndex++) {
      Integer length = lengths.get(lengthIndex);
      for (int i = 0; i < length / 2; i++) {
        Integer roundI = (i + position) % 256;
        Integer temp = list.get(roundI);
        list.set(roundI, list.get((position + length - i - 1) % 256));
        list.set((position + length - i - 1) % 256, temp);
      }
      position += (skipSize + length);
      position = position % 256;
      skipSize++;
    }
    return list;
  }
}
