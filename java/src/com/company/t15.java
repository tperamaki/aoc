package com.company;

import java.util.ArrayList;

public class t15 {
  private static final Long genAFactor = 16807L;
  private static final Long genBFactor = 48271L;
  private static final Long genDivider = 2147483647L;
  private static final Long genAStartValue = 289L;
  private static final Long genBStartValue = 629L;
  // 588 pairs
//  private static final Long genAStartValue = 65L;
//  private static final Long genBStartValue = 8921L;

  public static void run() {
    t1();
    t2();
  }

  private static void t1() {
    Long genAValue = genAStartValue;
    Long genBValue = genBStartValue;
    int pairs = 0;
    for (int i = 0; i < 40*1000*1000; i++) {
      genAValue = genAValue * genAFactor % genDivider;
      genBValue = genBValue * genBFactor % genDivider;
      String binA = Long.toBinaryString(genAValue);
      while (binA.length() < 16) {
        binA = "0" + binA;
      }
      binA = binA.substring(binA.length()-16);
      String binB = Long.toBinaryString(genBValue);
      while (binB.length() < 16) {
        binB = "0" + binB;
      }
      binB = binB.substring(binB.length()-16);
      if (binA.equals(binB))
        pairs++;
    }

    System.out.println(pairs);
  }

  private static void t2() {
    final int multiplierA = 4;
    final int multiplierB = 8;
    Long genAValue = genAStartValue;
    Long genBValue = genBStartValue;
    ArrayList<String> aValues = new ArrayList<>();
    ArrayList<String> bValues = new ArrayList<>();
    int pairs = 0;
    while (aValues.size() < 5*1000*1000) {
      genAValue = genAValue * genAFactor % genDivider;
      if (genAValue % multiplierA != 0) {
        continue;
      }
      String binA = Long.toBinaryString(genAValue);
      while (binA.length() < 16) {
        binA = "0" + binA;
      }
      binA = binA.substring(binA.length() - 16);
      aValues.add(binA);
    }

    while (bValues.size() < 5*1000*1000) {
      genBValue = genBValue * genBFactor % genDivider;
      if (genBValue % multiplierB != 0) {
        continue;
      }
      String binB = Long.toBinaryString(genBValue);
      while (binB.length() < 16) {
        binB = "0" + binB;
      }
      binB = binB.substring(binB.length()-16);
      bValues.add(binB);
    }

    for (int i = 0; i < aValues.size(); i++) {
      if (aValues.get(i).equals(bValues.get(i)))
        pairs++;
    }
    System.out.println(pairs);
  }

}
