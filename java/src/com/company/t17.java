package com.company;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

class t17 {
//  private static final int input = 3; // 2017 -> 638
  private static final int input = 366;

  private static ArrayList<Integer> inputData;

  static void run() {
    t1();
    t2();
  }


  private static void t1() {
    inputData = new ArrayList<>();
    inputData.add(0);
    inputData.add(1);
    int index = 1;
    for (int i = 2; i < 2018; i++) {
      index = (index+input) % inputData.size();
      inputData.add(++index, i);
    }
    System.out.println(inputData.get(index));
    System.out.println(inputData.get(index+1));
  }

  private static int processedAmount;

  private static void t2() {
    Date start = new Date();
    new Thread(() -> {
      while (processedAmount < 50000000) {
        System.out.println((processedAmount * 1.0 / 50000000 * 100) + "% in " + (new Date().getTime() - start.getTime()) + " ms");
        try {
          Thread.sleep(10000);
        } catch (InterruptedException e) {
          System.out.println("INTERRUPTED" + Arrays.toString(e.getStackTrace()));
        }
      }
    }).start();

    inputData = new ArrayList<>();
    inputData.add(0);
    inputData.add(1);
    int index = 1;
    for (processedAmount = 2; processedAmount < 50000000; processedAmount++) {
      index = (index+input) % inputData.size();
      inputData.add(++index, processedAmount);
    }
    System.out.println(inputData.get(inputData.indexOf(0)+1));
  }
}
