const fs = require('fs');
const util = require('util');
const readFile = util.promisify(fs.readFile);

async function readInput(fileName) {
  return await readFile(fileName, 'utf8');
};

async function t2016_01_1() {
    let data = await readInput("../input2016-01-1.txt");
    let arrayOfDirections = data.split(", ");
    let direction = 0;
    let x = 0
    let y = 0;
    arrayOfDirections.forEach(element => {
      if (element.substring(0, 1) == 'L') {
        //console.log("left");
        direction -= 90;
      } else {
        //console.log("right");
        direction += 90;
      }
      let distance = parseInt(element.substring(1));
      switch (direction % 360) {
        case 90:
        case -270:
          //console.log("east");
          x += distance;
          break;
        case 180:
        case -180:
          //console.log("south");
          y -= distance;
          break;
        case 270:
        case -90:
          //console.log("west");
          x -= distance;
          break;
        case 0:
        default:
          //console.log("north");
          y += distance;
          break;
      }
      //console.log(element + " -> " + x + "," + y);
    });
    //console.log(x+','+y);
    return Math.abs(x) + Math.abs(y);
  };
  
async function t2016_01_2() {
  let data = await readInput("../input2016-01-1.txt");
  let arrayOfDirections = data.split(", ");
  let direction = 0;
  let x = 0
  let y = 0;
  let arrayOfVisitedLocations = [];
  arrayOfVisitedLocations.push({x: 0, y: 0});
  arrayOfDirections.forEach(element => {
      if (element.substring(0, 1) == 'L') {
      //console.log("left");
      direction -= 90;
      } else {
      //console.log("right");
      direction += 90;
      }
      let distance = parseInt(element.substring(1));
      let location = {x: x, y: y};
      switch (direction % 360) {
      case 90:
      case -270:
          for (let i = 1; i<=distance; i++) {
          //console.log("east");
          arrayOfVisitedLocations.forEach(loc => {
              if (loc.x == location.x+i && loc.y == location.y) {
              console.log(location.x+i);
              console.log(location.y);
              //console.log(Math.abs(x+i) + Math.abs(y));
              }
          });
          arrayOfVisitedLocations.push({x: location.x+i, y: location.y});
          }
          x += distance;
          break;
      case 180:
      case -180:
          for (let i = 1; i<=distance; i++) {
          //console.log("south");
          arrayOfVisitedLocations.forEach(loc => {
              if (loc.x == location.x && loc.y == location.y-i) {
              //console.log(location.x);
              //console.log(location.y-i);
              console.log(Math.abs(x) + Math.abs(y-i));
              }
          });
          arrayOfVisitedLocations.push({x: location.x, y: location.y-i});
          }
          y -= distance;
          break;
      case 270:
      case -90:
          for (let i = 1; i<=distance; i++) {
          //console.log("west");
          arrayOfVisitedLocations.forEach(loc => {
              if (loc.x == location.x-i && loc.y == location.y) {
              //console.log(location.x-i);
              //console.log(location.y);
              console.log(Math.abs(x-i) + Math.abs(y));
              }
          });
          arrayOfVisitedLocations.push({x: location.x-i, y: location.y});
          }
          x -= distance;
          break;
      case 0:
      default:
          for (let i = 1; i<=distance; i++) {
          //console.log("north");
          arrayOfVisitedLocations.forEach(loc => {
              if (loc.x == location.x && loc.y == location.y+i) {
              //console.log(location.x);
              //console.log(location.y+i);
              console.log(Math.abs(x) + Math.abs(y+i));
              }
          });
          arrayOfVisitedLocations.push({x: location.x, y: location.y+1});
          }
          y += distance;
          break;
      }
      location = {x: x, y: y};
      //console.log(element + " -> " + x + "," + y);
  });
  //console.log(x+','+y);
  return Math.abs(x) + Math.abs(y);
  };


  module.exports = {
    t2016_01_1: t2016_01_1,
    t2016_01_2: t2016_01_2
  };
