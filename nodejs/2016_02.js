const fs = require('fs');
const util = require('util');
const readFile = util.promisify(fs.readFile);

async function readInput(fileName) {
  return await readFile(fileName, 'utf8');
};

function handleDirection1(direction, value) {
  switch(direction) {
    case 'L':
      if (value != 1 && value != 4 && value != 7)
        value--;
      break;
    case 'R':
      if (value != 3 && value != 6 && value != 9)
        value++;
      break;
    case 'U':
      if (value > 3)
        value -= 3;
      break;
    case 'D':
      if (value < 7)
        value += 3;
      break;
  }
  return value;
};

const keypad = {
  '1': { U: '1', D: '3', L: '1', R: '1' },
  '2': { U: '2', D: '6', L: '2', R: '3' },
  '3': { U: '1', D: '7', L: '2', R: '4' },
  '4': { U: '4', D: '8', L: '3', R: '4' },
  '5': { U: '5', D: '5', L: '5', R: '6' },
  '6': { U: '2', D: 'A', L: '5', R: '7' },
  '7': { U: '3', D: 'B', L: '6', R: '8' },
  '8': { U: '4', D: 'C', L: '7', R: '9' },
  '9': { U: '9', D: '9', L: '8', R: '9' },
  'A': { U: '6', D: 'A', L: 'A', R: 'B' },
  'B': { U: '7', D: 'D', L: 'A', R: 'C' },
  'C': { U: '8', D: 'C', L: 'B', R: 'C' },
  'D': { U: 'B', D: 'D', L: 'D', R: 'D' }
};

function handleDirection2(direction, value) {
  return keypad[value][direction];
};

async function t1() {
  let data = await readInput("../input2016-02-1.txt");
  let value = 5;
  let answer = "";
  
  data.split("\n").forEach(row => {
    row.split("").forEach(direction => {
      value = handleDirection1(direction, value);
    });
    answer += value;
  });
  return answer;
};

async function t2() {
  let data = await readInput("../input2016-02-1.txt");
  let value = 5;
  let answer = "";
  
  data.split("\n").forEach(row => {
    row.split("").forEach(direction => {
      value = handleDirection2(direction, value);
    });
    answer += value;
  });
  return answer;
};

module.exports = {
  t1: t1,
  t2: t2
};
