const fs = require('fs');
const util = require('util');
const readFile = util.promisify(fs.readFile);

async function readInput(fileName) {
  return await readFile(fileName, 'utf8');
};

const keypad = {
  '1': { U: '1', D: '3', L: '1', R: '1' },
  '2': { U: '2', D: '6', L: '2', R: '3' },
  '3': { U: '1', D: '7', L: '2', R: '4' },
  '4': { U: '4', D: '8', L: '3', R: '4' },
  '5': { U: '5', D: '5', L: '5', R: '6' },
  '6': { U: '2', D: 'A', L: '5', R: '7' },
  '7': { U: '3', D: 'B', L: '6', R: '8' },
  '8': { U: '4', D: 'C', L: '7', R: '9' },
  '9': { U: '9', D: '9', L: '8', R: '9' },
  'A': { U: '6', D: 'A', L: 'A', R: 'B' },
  'B': { U: '7', D: 'D', L: 'A', R: 'C' },
  'C': { U: '8', D: 'C', L: 'B', R: 'C' },
  'D': { U: 'B', D: 'D', L: 'D', R: 'D' }
};

function handleDirection2(direction, value) {
  return keypad[value][direction];
};

async function t1() {
  let data = await readInput("../input2016-04-1.txt");
  data.split("\n").forEach(row => {
    let code = row.substring(0, row.lastIndexOf("-"));
    let values = [];
    code.split("").forEach(letter => {
      if (letter != '-') {
        // do stuff
      }
    });
  });
  return "asd";
};

function isTrianglePossible(triangle) {
  return (triangle[0] < triangle[1]+triangle[2]
    && triangle[1] < triangle[0]+triangle[2]
    && triangle[2] < triangle[0]+triangle[1]);
};

async function t2() {
  let data = await readInput("../input2016-03-1.txt");
  let possible = [];
  let i = 0;
  let triangle1 = {};
  let triangle2 = {};
  let triangle3 = {};
  data.split("\n").forEach(row => {
    triangle1[i] = parseInt(row.substring(2,5));
    triangle2[i] = parseInt(row.substring(7,10));
    triangle3[i] = parseInt(row.substring(12,55));
    i++;
    if (i == 3) {
      i = 0;
      if (isTrianglePossible(triangle1)) {
        possible.push(triangle1);
      }
      if (isTrianglePossible(triangle2)) {
        possible.push(triangle2);
      }
      if (isTrianglePossible(triangle3)) {
        possible.push(triangle3);
      }
    }
  });
  return possible.length;
};


module.exports = {
  t1: t1,
  t2: t2
};
