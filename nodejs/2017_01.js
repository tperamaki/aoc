const fs = require('fs');
const util = require('util');
const readFile = util.promisify(fs.readFile);

async function readInput(fileName) {
  return await readFile(fileName, 'utf8');
};

async function t1() {
  let data = await readInput("../input2017-01-1.txt");
  let sum = 0;
  let previous = parseInt(data.substring(data.length-1));
  data.split("").forEach(element => {
    if (parseInt(element) == previous) {
      sum += previous;
    }
    previous = parseInt(element);
  });
  return sum;
};

async function t2() {
  let data = await readInput("../input2017-01-1.txt");
  let sum = 0;
  let firstHalf = data.substring(0, data.length/2).split("");
  let secondHalf = data.substring(data.length/2).split("");
  firstHalf.forEach((element, i) => {
    let value = parseInt(element);
    if (value == parseInt(secondHalf[i])) {
      sum += value;
    }
  });
  return sum*2;
};

module.exports = {
  t1: t1,
  t2: t2
};
