const fs = require('fs');
const util = require('util');
const readFile = util.promisify(fs.readFile);

async function readInput(fileName) {
  return await readFile(fileName, 'utf8');
};

async function t1() {
  let data = await readInput("../input2017-02-1.txt");
  let sum = 0;
  data.split("\n").forEach(row => {
    let smallest = 1000000;
    let largest = 0;
    row.split("\t").forEach(value => {
      value = parseInt(value);
      if (value < smallest) 
        smallest = value;
      if (value > largest)
        largest = value;
    });
    sum += largest-smallest;
  });
  return sum;
};

async function t2() {
  let data = await readInput("../input2017-02-1.txt");
  let sum = 0;
  data.split("\n").forEach(row => {
    let previousValues = [];
    row.split("\t").forEach(value => {
      value = parseInt(value);
      previousValues.forEach(prev => {
        if (prev % value == 0) {
          sum += prev / value;
        }
        if (value % prev == 0) {
          sum += value / prev;
        }
      })
      previousValues.push(value);
    });
  });
  return sum;
};


module.exports = {
  t1: t1,
  t2: t2
};
