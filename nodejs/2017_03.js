const fs = require('fs');
const util = require('util');
const readFile = util.promisify(fs.readFile);

async function readInput(fileName) {
  return await readFile(fileName, 'utf8');
};

async function t1() {
  data = 361527;
  let i;
  let value = data-1;
  for (i = 1; value > i*8; i++) {
    value -= i*8;
  }
  let loop = (i+1);
  let side = (i*2+1);
  let placeOnSide = value % (side-1);
  placeOnSide = placeOnSide > (side+1)/2 ? placeOnSide-((side+1)/2) : placeOnSide;
  return placeOnSide + loop;
};

async function t2() {
  for (let i = 0; (i+1) * (i+1) < 371527 ; i++) {
    console.log(((i+1) * (i+1)) + ", " + (i*i - (i-1)) + ", " + (i*i + 1) + ", " + (i*i + (i+1)));
  }
};


module.exports = {
  t1: t1,
  t2: t2
};
