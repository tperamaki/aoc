const fs = require('fs');
const util = require('util');
const readFile = util.promisify(fs.readFile);
const _ = require('lodash');

async function readInput(fileName) {
  return await readFile(fileName, 'utf8');
};

async function t1() {
  let data = await readInput("../input2017-04-1.txt");
  let sum = 0;
  data.split("\n").forEach(row => {
    let rowArray = row.split(" ");
    if (rowArray.length == _.uniq(rowArray).length) {
      sum++;
    }
  });
  return sum;
};

async function t2() {
  let data = await readInput("../input2017-04-1.txt");
  let sum = 0;
  data.split("\n").forEach(row => {
    let rowArray = row.split(" ");
    rowArray = rowArray.map(value => {
      let valueArray = value.split("").sort();
      let word = "";
      valueArray.forEach(letter => {
        word += letter;
      })
      return word;
    });
    if (rowArray.length == _.uniq(rowArray).length) {
      sum++;
    }
  });
  return sum;
};


module.exports = {
  t1: t1,
  t2: t2
};
