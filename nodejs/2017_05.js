const fs = require('fs');
const util = require('util');
const readFile = util.promisify(fs.readFile);
const _ = require('lodash');

async function readInput(fileName) {
  return await readFile(fileName, 'utf8');
};

async function t1() {
  let data = await readInput("../input2017-05-1.txt");
  let jumps = 0;
  let array = data.split("\n").map(row => {
    return parseInt(row);
  });

  let index = 0;
  for (jumps = 0; index >= 0 && index < array.length; jumps++) {
    let oldIndex = index;
    index += array[index];
    array[oldIndex]++;
  }
  return jumps;
};

async function t2() {
  let data = await readInput("../input2017-05-1.txt");
  let jumps = 0;
  let array = data.split("\n").map(row => {
    return parseInt(row);
  });

  let index = 0;
  for (jumps = 0; index >= 0 && index < array.length; jumps++) {
    let oldIndex = index;
    index += array[index];
    if (array[oldIndex] >= 3) {
      array[oldIndex]--;
    } else {
      array[oldIndex]++;
    }
  }
  return jumps;
};

module.exports = {
  t1: t1,
  t2: t2
};
