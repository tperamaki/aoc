const fs = require('fs');
const util = require('util');
const readFile = util.promisify(fs.readFile);
const _ = require('lodash');

async function readInput(fileName) {
  return await readFile(fileName, 'utf8');
};

async function t1() {
  let data = await readInput("../input2017-06.txt");
  let jumps = 0;
  let array = data.split("\t").map(row => {
    return parseInt(row);
  });

  let seenArrays = [];
  let arrayLength = array.length-1;
  for (jumps = 0; seenArrays.indexOf(array.toString()) == -1; jumps++) {
    seenArrays.push(array.toString());
    let highest = -1;
    array.forEach(value => { if (value > highest) { highest = value; }});
    let indexOfHighest = array.indexOf(highest);
    array[indexOfHighest] = 0;
    for (let i = indexOfHighest+1; highest > 0; i++, highest--) {
      if (i > arrayLength) {
        i = 0;
      }
      array[i] = array[i] + 1;
    }
  }
  return jumps;
};

async function t2() {
  let data = await readInput("../input2017-06.txt");
  let jumps = 0;
  let array = data.split("\t").map(row => {
    return parseInt(row);
  });

  let seenArrays = [];
  let arrayLength = array.length-1;
  for (jumps = 0; seenArrays.indexOf(array.toString()) == -1; jumps++) {
    seenArrays.push(array.toString());
    let highest = -1;
    array.forEach(value => { if (value > highest) { highest = value; }});
    let indexOfHighest = array.indexOf(highest);
    array[indexOfHighest] = 0;
    for (let i = indexOfHighest+1; highest > 0; i++, highest--) {
      if (i > arrayLength) {
        i = 0;
      }
      array[i] = array[i] + 1;
    }
  }

  seenArrays = [];

  for (jumps = 0; seenArrays.indexOf(array.toString()) == -1; jumps++) {
    if (jumps == 0)
      seenArrays.push(array.toString());
    let highest = -1;
    array.forEach(value => { if (value > highest) { highest = value; }});
    let indexOfHighest = array.indexOf(highest);
    array[indexOfHighest] = 0;
    for (let i = indexOfHighest+1; highest > 0; i++, highest--) {
      if (i > arrayLength) {
        i = 0;
      }
      array[i] = array[i] + 1;
    }
  }

  return jumps;
};


module.exports = {
  t1: t1,
  t2: t2
};
