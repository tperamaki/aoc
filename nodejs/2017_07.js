const fs = require('fs');
const util = require('util');
const readFile = util.promisify(fs.readFile);
const _ = require('lodash');

async function readInput(fileName) {
  return await readFile(fileName, 'utf8');
};

async function t1() {
  let data = await readInput("../input2017-07.txt");
  let possibleBottoms = [];
  let possibleBottoms2 = [];
  data.split("\n").forEach(row => {
    if (row.indexOf(">") > 0) {
      possibleBottoms.push(row);
    }
  });

  console.log(data.split("\n").length);

  possibleBottoms2 = possibleBottoms.filter(row => {
    possibleBottoms.forEach(row2 => {
      if (row2.split(" -> ")[1].split(", ").indexOf(row) != -1) {
        return false;
      }
    });

    return true;
  });
  console.log(possibleBottoms.length);

  return possibleBottoms2[0];
};

function thisRowValue(row) {
  let value = row.substring(row.indexOf('(')+1, row.indexOf(')'));
  return parseInt(value);
}

function valuesOfChildren(row, data) {
  let value = thisRowValue(row);
  if (row.indexOf('>') != -1) {
    row.split("> ")[1].split(", ").forEach(child => {
      let childRow;
      data.forEach(dRow => {
        if (dRow.indexOf(child) == 0) {
          childRow = dRow;
        }
      });
      value += valuesOfChildren(childRow, data);
      //if (row.indexOf("tylelk") == 0 || row.indexOf("drfzng") == 0 || row.indexOf("yhonqw") == 0 || row.indexOf("wsyiyen") == 0 || row.indexOf("dqwocyn") == 0 || row.indexOf("qqnroz") == 0) {
      if (row.indexOf("dqwocyn") == 0 ) {
        console.log(row + " == " + valuesOfChildren(childRow, data));
      }
    });
  }

  return value;
}

function valuesOfChildrenMatch(childs, data) {
  let value = 0;
  let answer = true;
  childs.forEach(child => {
    let thisRow;
    data.forEach(row => {
      if (row.indexOf(child) == 0) {
        thisRow = row;
      }
    });
    let rowValue = 0;
    rowValue += valuesOfChildren(thisRow, data);
    if (value == 0) {
      value = rowValue;
    } else if (value != rowValue) {
      answer = false;
    }
  });
  return answer;
}

async function t2() {
  let data = await readInput("../input2017-07.txt");
  data = data.split("\n");
  let incorrectRows = [];
  let smallest = 99999999;

  data.forEach(row => {
    let splittedRow = row.split("> ");
    if (splittedRow.length > 1) {
      let childs = splittedRow[1].split(", ");
      if (!valuesOfChildrenMatch(childs, data)) {
        incorrectRows.push(splittedRow);
        if (smallest > thisRowValue(row)) {
          smallest = thisRowValue(row);
        }
      }
    }
  });

  return incorrectRows;
};


module.exports = {
  t1: t1,
  t2: t2
};
