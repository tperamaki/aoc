const fs = require('fs');
const util = require('util');
const readFile = util.promisify(fs.readFile);
const _ = require('lodash');

async function readInput(fileName) {
  return await readFile(fileName, 'utf8');
};

function isConditionTrue(registry, row) {
  switch (row[5]) {
    case ">":
      if (registry[row[4]].value > parseInt(row[6])) {
        return true;
      }
      break;
    case "<":
      if (registry[row[4]].value < parseInt(row[6])) {
        return true;
      }
      break;
    case "==":
      if (registry[row[4]].value == parseInt(row[6])) {
        return true;
      }
      break;
    case ">=":
      if (registry[row[4]].value >= parseInt(row[6])) {
        return true;
      }
      break;
      case "<=":
        if (registry[row[4]].value <= parseInt(row[6])) {
          return true;
        }
        break;
      case "!=":
        if (registry[row[4]].value != parseInt(row[6])) {
          return true;
        }
        break;
  }
  return false;
}

async function t1() {
  let data = await readInput("../input2017-08.txt");
  let registry = {};
  _.uniq(_.flatMap(data.split("\n").map(row => row.split(" ")[0]))).forEach(name => {
    registry[name] = { value: 0 };
  });

  let largest = 0;
  data.split("\n").forEach(row => {
    row = row.split(" ");
    let value = 0;
    if (isConditionTrue(registry, row)) {
      if (row[1] == "inc") {
        registry[row[0]].value += parseInt(row[2]);
      } else {
        registry[row[0]].value -= parseInt(row[2]);
      }
      value = registry[row[0]].value;
      if (value > largest) {
        largest = value;
      }
    }
  });

  console.log(registry);
  return largest;
};

async function t2() {
  return await t1();
};


module.exports = {
  t1: t1,
  t2: t2
};
