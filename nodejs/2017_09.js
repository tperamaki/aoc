const fs = require('fs');
const util = require('util');
const readFile = util.promisify(fs.readFile);
const _ = require('lodash');

async function readInput(fileName) {
  return await readFile(fileName, 'utf8');
};

async function findChar(data, char) {
  let index;
  for (index = data.indexOf(char); index != -1 && data[index-1] == '!';) {
    let cancelled = 1;
    for (let i = 1; data[index-1-i] == '!'; i++) {
      cancelled++;
    }
    if (cancelled % 2 == 0) {
      break;
    }
    index = data.indexOf(char, ++index);
  }

  return index;
}

async function getCleanedSum(data, start, stop) {
  let sum = 0;
  let prevChar = '';
  let cleanedData = data.substring(start+1, stop);
  cleanedData.split("").forEach(char => {
    if (prevChar != '!') {
      if (char != '!')
        sum++;
      prevChar = char;
    } else {
      prevChar = '';
    }
  });
  return sum;
};

async function cleanData(data) {
  let sum = 0;
  while (true) {
    let start = await findChar(data, '<');
    let stop = await findChar(data, '>');
    if (start == -1)
      break;
    sum += await getCleanedSum(data, start, stop);
    data = data.substring(0, start) + data.substring(stop+1);
  }
  console.log("cleaned sum", sum);
  return data;
};

async function getSum(data) {
  let sum = 0;
  let value = 1;
  data.forEach(char => {
    if (char == '{') {
      sum += value++;
    } else if (char == '}') {
      value--;
    }
  })
  return sum;
};

async function t1() {
  let data = await readInput("../input2017-09.txt");
  data = await cleanData(data);
  return await getSum(data.split(""));
};

async function t2() {
  return await t1();
};


module.exports = {
  t1: t1,
  t2: t2
};
