const fs = require('fs');
const util = require('util');
const readFile = util.promisify(fs.readFile);
const _ = require('lodash');
let input = "83,0,193,1,254,237,187,40,88,27,2,255,149,29,42,100";

async function t1() {
  let data = input;
  data = await cleanData(data);
  return await getSum(data.split(""));
};

async function t2() {
  return await t1();
};


module.exports = {
  t1: t1,
  t2: t2
};
