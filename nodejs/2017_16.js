const fs = require('fs');
const util = require('util');
const readFile = util.promisify(fs.readFile);
const _ = require('lodash');

const AMOUNT_OF_PROGRAMS = 16;
//const AMOUNT_OF_PROGRAMS = 5;

let listOfCommands = [];
let programs = "";
let initialized = false;

async function readInput(fileName) {
  return await readFile(fileName, 'utf8');
};

async function t1() {
  if (!initialized) {
    initialized = true;
    programs = [];
    for (let i = 0; i < AMOUNT_OF_PROGRAMS; i++) {
      programs += String.fromCharCode('a'.charCodeAt(0) + i);
    }
    let data = await readInput("../input2017-16.txt");
    //data = "s1,x3/4,pe/b";
    await initializeListOfCommands(data);
  }

  doCommands();
  return programs;
};

async function initializeListOfCommands(data) {
  listOfCommands = [];
  data.split(",").forEach(element => {
    listOfCommands.push({
      command: element[0],
      value1: element.substring(1).split("/")[0],
      value2: element.split("/")[1]
    });
  });
};

async function doCommands() {
  listOfCommands.forEach(command => {
    if (programs.length == 1)
      return;
    switch (command.command) {
      case 's':
        spin(parseInt(command.value1));
        break;
      case 'x':
        exchance(parseInt(command.value1), parseInt(command.value2));
        break;
      case 'p':
        partner(command.value1, command.value2);
        break;
      default:
        System.out.println("ERROR!!!!");
    }
  });
};

async function spin(value1) {
  value1 = value1 % programs.length;
  programs = programs.substring(programs.length - value1) + programs;
  programs = programs.substring(0, programs.length - value1, programs.length);
};

async function exchance(value1, value2) {
  let tempChar = programs.charAt(value1);
  programs = (programs.substring(0, value1) + programs.charAt(value2) + programs.substring(value1 + 1));
  programs = (programs.substring(0, value2) + tempChar + programs.substring(value2 + 1));
};

async function partner(value1, value2) {
  let indexA = programs.indexOf(value1);
  let indexB = programs.indexOf(value2);
  exchance(indexA, indexB);
};

async function t2() {
  for (let i = 1; i < 1000000000; i++) { // 1000000000
    await t1();
    report(i);
  };
  return programs;
};

async function report(i) {
  if (i % 1000 == 0)
    console.log((i * 1.0 / 1000000000 * 100) + "%");
}


module.exports = {
  t1: t1,
  t2: t2
};

/*
private static final int AMOUNT_OF_PROGRAMS = 16;
//  private static final String input = "s1,x3/4,pe/b";
//  private static final int AMOUNT_OF_PROGRAMS = 5;

  private static StringBuilder programs = new StringBuilder();
  private static ArrayList<String> listInput = new ArrayList<>(Arrays.asList(input.split(",")));

  private static String answer1 = "";
  private static String answer2 = "";
  private static List<Operation> listOfCommands;

  static void run() {
    for (int i = 0; i < AMOUNT_OF_PROGRAMS; i++)
      programs.append(((char) ('a' + i)));
    initializeListOfCommands();

    t1();
    System.out.println(programs);

    t2();
    System.out.println(programs);
  }

  private static void t1() {
    listOfCommands.forEach(t16::handleCommand);
  }

  private static void t2() {
    Date start = new Date();
    for (int i = 1; i < 1000000000; i++) {
      t1();
      if (i % 10000 == 0) {
        System.out.println((i * 1.0 / 1000000000 * 100) + "% in " + (new Date().getTime() - start.getTime()) + " ms");
      }
    }
  }

  private static void initializeListOfCommands() {
    listOfCommands =  listInput.stream().map(command -> {
      switch (command.charAt(0)) {
        case 's':
          return new Operation('s', Integer.parseInt(command.substring(1)));
        case 'x':
          return new Operation('x', Integer.parseInt(command.substring(1).split("/")[0]), Integer.parseInt(command.split("/")[1]));
        case 'p':
          return new Operation('p', command.charAt(1), command.charAt(3));
      }
      System.out.println("ERROR");
      return null;
    }).collect(Collectors.toList());
  }

  private static class Operation {
    char command;
    int intParameterA;
    int intParameterB;
    String charParameterA;
    String charParameterB;

    Operation(char c, int a, int b) {
      command = c;
      intParameterA = a;
      intParameterB = b;
    }

    Operation(char c, char a, char b) {
      command = c;
      charParameterA = a + "";
      charParameterB = b + "";
    }

    Operation(char c, int a) {
      command = c;
      intParameterA = a;
    }
  }

  private static void handleCommand(Operation command) {
    switch (command.command) {
      case 's':
        spin(command.intParameterA);
        break;
      case 'x':
        exchance(command.intParameterA, command.intParameterB);
        break;
      case 'p':
        partner(command.charParameterA, command.charParameterB);
        break;
      default:
        System.out.println("ERROR!!!!");
    }
  }

  private static void spin(int amount) {
    spin1(amount);
  }

  private static void spin1(int amount) {
    amount = amount % programs.length();
    programs.insert(0, programs.substring(programs.length() - amount));
    programs.delete(programs.length() - amount, programs.length());
  }

  private static void spin2(int amount) {
    int startIndex = programs.length()-(amount % programs.length());
    if (startIndex == 0) {
      return;
    }

    StringBuilder newPrograms = new StringBuilder();
    for (int i = 0; i < programs.length(); i++) {
      newPrograms.append(programs.charAt((startIndex + i) % programs.length()));
    }
    programs = newPrograms;
  }

  private static void exchance(int a, int b) {
    char tempChar = programs.charAt(a);
    programs.setCharAt(a, programs.charAt(b));
    programs.setCharAt(b, tempChar);
  }

  private static void partner(String a, String b) {
    int indexA = programs.indexOf(a);
    int indexB = programs.indexOf(b);
    exchance(indexA, indexB);
  }
}
*/