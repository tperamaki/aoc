const t2016 = require('./2016_04.js');
const t2017 = require('./2017_16.js');

async function main() {
  console.log("16_t1 == " + await t2016.t1());
  console.log("16_t2 == " + await t2016.t2());

  console.log("17_t1 == " + await t2017.t1());
  console.log("17_t2 == " + await t2017.t2());
};

main();